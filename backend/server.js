// .env file read
import dotenv from 'dotenv';

// Import Config

// import route

// import package
import express from 'express';
import path from 'path';


// define _dirname
import { fileURLToPath } from 'url';
import route from './lib/route/index.js';
import config from './lib/config/index.js';

import loggerh from './lib/logger/index.js';

console.log('');
console.log('//************************* My-App 1.0.0 **************************//');
console.log('Server Start Date : ', new Date());
// dotenv.config({ debug: process.env.DEBUG });
dotenv.config();
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const { logger } = loggerh;
import cors from 'cors';


// config.dbConfig()
config.dbConfig.connectDb(config.cfg, (error) => {
  if (error) {
    logger.error(error, 'Exiting the app.');
    return error;
  }
  // init express app
  const app = express();

  app.use(cors('*'));

  // config express
  config.expressConfig(app, config.cfg.environment);

  // use route
  route(app);

  // set the view engine to ejs
  app.set('views', `${__dirname}/views`);
  app.set('view engine', 'ejs');

  // set server home directory
  app.locals.rootDir = __dirname;

  // start server
  app.listen(config.cfg.port, () => {
    logger.info(`Express server listening on ${config.cfg.ip}:${config.cfg.port}, in ${config.cfg.TAG} mode`);
  });
});
