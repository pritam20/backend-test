import bunyan from 'bunyan';
import _ from 'lodash';
import path from 'path';
import * as dotenv from 'dotenv';
// define _dirname
import { fileURLToPath } from 'url';

import _debug from 'debug';
import bunyanDebugStream from 'bunyan-debug-stream';

dotenv.config();
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let logger; let
  streams;

const TAG = {
  REQUEST: 'Request',
  RESPONSE: 'Response',
  DEBUG: 'Debug',
  ERROR: 'Error',
  MESSAGE: 'Message',
};

const initialize = _.once(() => {
  if (process.env.NODE_ENV === 'prod') {
    const stream = bunyanDebugStream.create({
      basepath: `${__dirname}/..`, // this should be the root folder of your project.
    });

    streams = [{
      level: 'error',
      type: 'raw',
      stream,
    }, {
      level: 'warn',
      type: 'raw',
      stream,
    }];
  } else {
    streams = [{
      level: 'error',
      path: 'app.log',
    }, {
      level: 'warn',
      path: 'app.log',
    }, {
      level: 'info',
      path: 'app.log',
    }];
  }

  logger = bunyan.createLogger({
    name: process.env.PROJECT_NAME,
    streams,
    serializers: {
      req: bunyan.stdSerializers.req,
      res: bunyan.stdSerializers.res,
      err: bunyan.stdSerializers.err,
    },
  });
});

initialize();

export default {
  logger,
};
