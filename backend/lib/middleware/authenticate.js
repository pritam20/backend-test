//= ========================= Load Modules Start ===========================
import mongoose from 'mongoose';
//= ========================= Load internal Module =========================

import redisSession from '../redisClient/session.js';
import customException from '../customException.js';

import userService from '../module/v1/user/userService.js';

//= ========================= Load Modules End =============================

const __verifyTok = async (acsTokn) => {
  try {
    return await redisSession.getByToken(acsTokn);
  } catch (err) {
    next(err);
  }
};

const expireToken = async (req, res, next) => {
  try {
    const acsToken = await req.get('accessToken');

    await redisSession.expire(acsToken);
    next();
  } catch (err) {
    next(err);
  }
};

const autntctTkn = async (req, res, next) => {
  try {
    const acsToken = await req.get('accessToken') ?? req.query.token;

    if (!acsToken) {
      return next(customException.unauthorizeAccess());
    }

    const tokenPayload = await __verifyTok(acsToken);

    if (tokenPayload.d) {
      const userId = new mongoose.Types.ObjectId(tokenPayload.d.userId);
      req.user = tokenPayload.d;
      req.user.userId = userId;
      next();
    } else {
      throw customException.unauthorizeAccess();
    }
  } catch (err) {
    next(err);
  }
};

const autntctTknAdm = async (req, res, next) => {
  try {
    const acsToken = await req.get('accessToken') ?? req.query.token;

    if (!acsToken) {
      return next(customException.unauthorizeAccess());
    }

    const tokenPayload = await __verifyTok(acsToken);

    if (tokenPayload.d) {
      const adminId = new mongoose.Types.ObjectId(tokenPayload.d.adminId);

      req.user = tokenPayload.d;
      req.user.adminId = adminId;
      next();
    } else {
      throw customException.unauthorizeAccess();
    }
  } catch (err) {
    next(err);
  }
};

const authSocketTkn = (socket, next) => {
  const { accessToken } = socket.handshake.query;

  if (!accessToken) {
    return next(customException.unauthorizeAccess());
  }

  next();
  __verifyTok(accessToken)
    .bind({})
    .then((tokenPayload) => {
      if (tokenPayload.d) {
        const paylod = tokenPayload.d;
        socket.payload = tokenPayload.d;

        return userService.getByKey({ _id: paylod.userId });
      }
      next();
    })
    .then((user) => {
      socket.user = user;
      next();
    })
    .catch((err) => {
      next(new Error('Authentication error'));
    });
};

//= ========================= Export Module Start ===========================

export default {
  autntctTknAdm,
  autntctTkn,
  expireToken,
};

//= ========================= Export Module End ===========================
