import fs from 'fs';
import { Parser } from 'json2csv';


const create = (data) => {

    const fields = [
        { label: 'country', value: 'country' },
        { label: 'airbnbCount', value: 'count' }
    ];

    const json2csvParser = new Parser({ fields });
    const csv = json2csvParser.parse(data);

    fs.writeFile('hotels.csv', csv, (err) => {
        if (err) {
            console.error('Error creating CSV file:', err);
            return;
        }
        console.warn('hotels.csv file created successfully. Please check in root folder');
    });


}

export default { create }