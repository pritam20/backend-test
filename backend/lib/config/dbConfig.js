//= ================================== Load Modules start ===================================

//= ================================== Load external modules=================================
import mongoose from 'mongoose';
import loggerh from '../logger/index.js'; // Initialize logger
import appUtils from '../appUtils.js';
import adminService from '../module/v1/admin/adminService.js';
import whiteListService from '../module/whiteList/whiteListService.js';
import config from './index.js';
// Import logger
const { logger } = loggerh;

//= ================================== Load Modules end =====================================

// Connect to Db
const connectDb = (async (env, callback) => {
  try {
    const { dbName } = env.mongo;
    let { dbUrl } = env.mongo;
    const dbOptions = env.mongo.options;

    if (env.debug === true || env.debug === 'true') {
      logger.info(`Configuring db in ${env.TAG} mode`);
      dbUrl += dbName;
      mongoose.set('debug', true);
    } else {
      logger.info(`Configuring db in ${env.TAG} mode`);
      dbUrl += dbName;
    }

    mongoose.connect(dbUrl, dbOptions);
    logger.info(`Connecting to -> ${dbUrl}`);

    // CONNECTION EVENTS
    // When successfully connected
    mongoose.connection.on('connected', () => {
      logger.info('Connected to DB', dbName, 'at', dbUrl);
      callback();
    });

    // If the connection throws an error
    mongoose.connection.on('error', (error) => {
      logger.info(`DB connection error: ${error}`);
      callback(error);
    });

    // When the connection is disconnected
    mongoose.connection.on('disconnected', () => {
      logger.info('DB connection disconnected.');
      callback('DB connection disconnected.');
    });

    // project run admin create
    mongoose.connection.on('open', () => {
      const param = {};
      param.email = config.cfg.adminEmail;
      param.password = appUtils.createHashSHA256(config.cfg.adminPassword);
      param.status = 1;
      param.userType = 1;
      adminService.createAdmin(param);

      whiteListService.createIP({ IP: '::1' });
    });
  } catch (err) {
    throw err;
  }
});

// ========================== Export Module Start ==========================
export default { connectDb };
// ========================== Export Module End ============================
