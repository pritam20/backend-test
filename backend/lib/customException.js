//= ========================= Load Modules Start ===========================

//= ========================= Load Internal Module =========================

// Load exceptions
import Exception from './model/Exception.js';
import constants from './constant.js';
//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

//----> 200 OK: The request was successful and the server returned the requested data.
//----> 201 Created: The request was successful and a new resource was created.
//----> 204 No Content: The request was successful, but there is no data to return.
//----> 400 Bad Request: The request was malformed or invalid.
//----> 401 Unauthorized: The request requires authentication, but the user is not authenticated.
//----> 403 Forbidden: The user is authenticated, but does not have permission to access the requested resource.
//----> 404 Not Found: The requested resource was not found on the server.
//----> 409 conflict error: In case of conflict
//----> 500 Internal Server Error: An error occurred on the server while processing the request

export default {
  intrnlSrvrErr: (err) => new Exception(500, constants.MESSAGES.INTERNAL_SERVER_ERROR, err),
  unauthorizeAccess: (err) => new Exception(401, constants.MESSAGES.UNAUTHORIZED_ACCESS, err),
  alreadyRegistered: (err) => new Exception(409, constants.MESSAGES.ALREADY_EXIST, err),
  invalidEmail: () => new Exception(400, constants.MESSAGES.INVALID_EMAIL),
  getCustomErrorException: (errMsg, error) => new Exception(400, errMsg, error),
  userNotFound: () => new Exception(404, constants.MESSAGES.USER_NOT_REGISTERED),
  wrongCredentials: () => new Exception(401, constants.MESSAGES.INCORRECT_PASS),
  otpNotValid: () => new Exception(401, constants.MESSAGES.WRONG_OTP),
  emailNotVerified: () => new Exception(403, constants.MESSAGES.EMAIL_NOT_VERIFIED),
  adminNotFound: () => new Exception(404, constants.MESSAGES.ADMIN_NOT_EXIST),
  productAvailable: () => new Exception(200, constants.MESSAGES.PRODUCT_AVAILABLE),
};

//= ========================= Export Module   End ===========================
