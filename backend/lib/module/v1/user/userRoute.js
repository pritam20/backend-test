import express from 'express';
import requestIp from 'request-ip';

import resHndlr from '../../../responseHandler.js';
import middleware from '../../../middleware/index.js';
import userFacade from './userFacade.js';
import validator from './userValidators.js';

const userRouter = express.Router();

//= =============================================================

userRouter.route('/signup')
  .post(middleware.multer.single('profileImage'), middleware.mediaUpload.uploadSingleMediaToS3('image'), [validator.validateSignUp], async (req, res) => {
    try {
      let profileImage;
      let profileImageData;

      const {
        name,
        email,
        password,
        username,
        userType,
        gender,
        dob,
        designation,
        companyName,
        phoneNo,
      } = req.body;

      const { user } = req;

      if (req.body.location) {
        profileImageData = req.file;
        profileImage = req.body.location;
      }

      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.create({
        name,
        email,
        password,
        username,
        userType,
        gender,
        dob,
        designation,
        companyName,
        phoneNo,
        clientIp,
        profileImage,
        profileImageData,
      });
      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/verifyEmail')
  .put([validator.validateVerifyEmail], async (req, res) => {
    try {
      const { email, otp } = req.query;

      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.verifyEmail({ email, otp, clientIp });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/resendOtp')
  .post([validator.validateResendOtp], async (req, res) => {
    try {
      const { email } = req.body;

      const result = await userFacade.otpResend(email);

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/login')
  .post([validator.validateLogin], async (req, res) => {
    try {
      const { email, password } = req.body;

      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.userLogin({ email, password, clientIp });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/socialSignup')
  .post([validator.validateSocialSignup], async (req, res) => {
    try {
      const {
        socialType, email, name, id, profilePicURL,
      } = req.body;
      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.socialSignup({
        socialType, email, name, id, profilePicURL, clientIp,
      });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/forgot')
  .post([validator.validateResendOtp], async (req, res) => {
    try {
      const { email } = req.body;

      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.forgetPassword({ email, clientIp });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/reset')
  .post([validator.validateReset], async (req, res) => {
    try {
      const { email, otp, password } = req.body;

      const clientIp = requestIp.getClientIp(req);

      const result = await userFacade.resetPassword({
        email, otp, password, clientIp,
      });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/logout')
  .post([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], (req, res) => {
    try {
      const result = userFacade.logout();

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

userRouter.route('/deleteAccount')
  .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], [validator.validateDeleteAccount], async (req, res) => {
    try {
      const { email } = req.query;

      const result = await userFacade.deleteAccount(email);

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

export default userRouter;
