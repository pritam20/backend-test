//= ========================= Load Modules Start =======================

//= ========================= Load internal modules ====================
import mongoose from 'mongoose';
import _ from 'lodash';
//= ========================= Load internal modules ====================
import userModel from './userModel.js';

// init user dao
import BaseDao from '../../../dao/baseDao.js';
import h from '../../../socket/socketChat.js';

const userDao = new BaseDao(userModel);
//= ========================= Load Modules End ==============================================

const isEmailExist = async (email) => {
  try {
    return await userDao.findOne({ email });
  } catch (err) {
    return err;
  }
};

const createUser = async (params) => {
  const query = {};

  query.name = params.name ?? '',
    query.email = params.email ?? '',
    query.password = params.password ?? '',
    query.username = params.username ?? '',
    query.userType = params.userType ?? 1,
    query.gender = params.gender ?? '',
    query.dob = params.dob ?? '',
    query.designation = params.designation ?? '',
    query.companyName = params.companyName ?? '',
    query.phoneNo = params.phoneNo ?? '',
    query.clientIp = params.clientIp ?? '';
  query.profileImage = params.profileImage ?? '';

  return await userDao.save(query);
};

const deleteUser = async (email) => await userDao.deleteOne({ email });

const verifyEmail = async (params) => {
  const status = 2;
  const query = { status };

  return await userDao.findOneAndUpdate({ email: params.email }, query);
};

const changePassword = async (params) => await userDao.findOneAndUpdate({ email: params.email }, { password: params.password });

const isSocialIdExist = async (id) => await userDao.findOne({ 'socialProfile.id': id });

const socialSignup = async (params) => {
  const query = {};

  query.email = params.email;
  query.name = params.name;
  query.socialProfile = { isSocial: true, socialType: params.socialType, id: params.id };

  return await userDao.save(query);
};

const getByKey = (query) => userDao.findOne(query);

//= ======================= Export Module Start ==============================

export default {
  createUser,
  verifyEmail,
  changePassword,
  getByKey,
  socialSignup,
  isEmailExist,
  deleteUser,
  isSocialIdExist,
};

//= ========================= Export Module End ===============================
