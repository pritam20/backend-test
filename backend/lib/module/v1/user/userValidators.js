//= ========================= Load Modules Start ===========================

//= ========================= Load external Module =========================
import _ from 'lodash';

//= ========================= Load Internal Module =========================
import Joi from 'joi';
import appUtils from '../../../appUtils.js';
import constant from '../../../constant.js';
import exceptions from '../../../customException.js';
//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

const validateSignUp = (req, res, next) => {
  console.log(req.body)

  const schema = Joi.object({
    name: Joi.string().min(3).max(50),
    email: Joi.string().email().required(),
    username: Joi.string().required(),
    userType: Joi.number(),
    gender: Joi.number(),
    dob: Joi.string().pattern(/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/\d{4}$/),
    designation: Joi.string().min(1).max(20),
    companyName: Joi.string().min(1).max(20),
    phoneNo: Joi.string().length(10),
    password: Joi.string().pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/).required(),
    profileImage: Joi.string().allow(''),
    location: Joi.string()

  });

  const { error, value } = schema.validate(req.body);


  if (error && error.details.length > 0) {

    console.log(error, '-------FGHJKL')

    validationError(error.details, next);
  } else {
    next();
  }
};

const validateLogin = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/).required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateVerifyEmail = (req, res, next) => {

  const schema = Joi.object({
    email: Joi.string().email().required(),
    otp: Joi.string().length(6).pattern(/^\d+$/).required(),
  });

  const { error, value } = schema.validate(req.query);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateResendOtp = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateSocialSignup = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    socialType: Joi.number().required(),
    name: Joi.string().required(),
    id: Joi.string().required(),
    profilePicURL: Joi.string().required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateReset = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    otp: Joi.string().length(6).pattern(/^\d+$/).required(),
    password: Joi.string().pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/).required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateDeleteAccount = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
  });

  const { error, value } = schema.validate(req.query);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validationError = (errors, next) => {
  if (errors && errors.length > 0) {
    return next(exceptions.getCustomErrorException(constant.MESSAGES.VALIDATION_ERROR, errors));

  }
  next();
};

export default {
  validateSignUp,
  validateLogin,
  validateVerifyEmail,
  validateResendOtp,
  validateSocialSignup,
  validateReset,
  validateDeleteAccount,
};
//= ========================= Export module end ==================================
