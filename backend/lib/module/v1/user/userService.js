//= ========================= Load Modules Start =======================
import appUtils from '../../../appUtils.js';
//= ========================= Load internal modules ====================
import userDao from './userDao.js';

//= ========================= Load Modules End ==============================================

const isEmailExist = (email) => userDao.isEmailExist(email);

const createUser = async(params) => {
  params.password = appUtils.encryptHashPassword(params.password); // change plain password to hash password
  const userDetails = await userDao.createUser(params);
  userDetails.otp = appUtils.getRandomOtp(); // add random otp to userdetails
  return userDetails;
};

const deleteUser = async(email) => await userDao.deleteUser(email);

const verifyEmail = async(params) => await userDao.verifyEmail(params);

const generateOtp = () => appUtils.getRandomOtp();

const changePassword = async(params) => {
  params.password = appUtils.encryptHashPassword(params.password); // change plain password to hash password
  return await userDao.changePassword(params);
};

const deleteAccount = (email) => userDao.deleteUser(email);

const isSocialIdExist = async(id) => await userDao.isSocialIdExist(id);

const socialSignup = async(params) => await userDao.socialSignup(params);

const getByKey = (param) => userDao.getByKey(param);

//= ========================= Export Module Start ==============================

export default {
  verifyEmail,
  socialSignup,
  deleteAccount,
  generateOtp,
  changePassword,
  createUser,
  isEmailExist,
  getByKey,
  deleteUser,
  isSocialIdExist,
};

//= ========================= Export Module End ===============================
