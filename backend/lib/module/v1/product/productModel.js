// Importing mongoose
import mongoose from 'mongoose';
import constants from '../../../constant.js';

const { Schema } = mongoose;
let Product;

const ProductSchema = new Schema(
  {
    pId: {
      type: String,
      uniqe: true,
      required: [true, "Please enter pid"]
    },
    name: {
      type: String,
      required: [true, 'Please enter product name'],
      trim: true
    },
    description: {
      type: String,
      required: [true, 'Please enter product description']
    },
    price: {
      type: Number,
      required: [true, 'Please enter product description'],
      maxLength: [8, "Price must be under 8 character"]
    },
    rating: {
      type: Number,
      default: 0
    },
    images: [{
      type: String
    }],
    category: {
      type: String,
      required: [true, 'Please enter product category']
    },
    stock: {
      type: Number,
      required: [true, 'Please enter product stock'],
      maxLength: [10, "stock shoul be less than 10"],
      default: 1
    },
    numOfReview: {
      type: Number,
      default: 0
    },
    reviews: [
      {
        name: {
          type: String,
          required: true
        },
        rating: {
          type: Number,
          required: true,
        },
        comment: {
          type: String,
          required: true
        }
      }
    ]

  },
  {
    timestamps: true,
  },
);

// Export user module
Product = mongoose.model(constants.DB_MODEL_REF.PRODUCT, ProductSchema);

export default Product;
