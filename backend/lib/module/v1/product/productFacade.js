//= ========================= Load Modules Start =======================

//= ========================= Load external modules ====================
// Load user service
import _ from 'lodash';
//= ========================= Load internal modules ====================

import productService from './productService.js';
import productMapper from './productMapper.js';

import appUtils from '../../../appUtils.js';
import redisSession from '../../../redisClient/session.js';
import customException from '../../../customException.js';
import constants from '../../../constant.js';
import emailService from '../../../service/sendgrid_email.js';
import redis from '../../../redisClient/init.js';
import mail from '../../../service/nodemailer_email.js';

//= ========================= Load Modules End ==============================================

const addProduct = async (params) => {
  try {

    const isProductAvailable = await productService.findProduct(params.pId)

    if (isProductAvailable) {
      throw customException.productAvailable()
    }

    await productService.addProduct(params)

    return productMapper.addProduct(params)

  }
  catch (err) {
    throw err
  }


}

//= ========================= Export Module Start ==============================

export default {
  addProduct
};

//= ========================= Export Module End ================================
