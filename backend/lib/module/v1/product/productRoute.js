import express from 'express';

import resHndlr from '../../../responseHandler.js';
import middleware from '../../../middleware/index.js';
import productFacade from './productFacade.js';
import validator from './productValidators.js';

const productRouter = express.Router();

//= =============================================================

productRouter.route('/addProduct')
  .post([middleware.authenticate.autntctTknAdm], [middleware.multer.single('image')], [middleware.mediaUpload.uploadSingleMediaToS3('image')], async (req, res) => {
    try {
      let images;
      const {
        pId,
        name,
        description,
        price,
        category,
        stock,
      } = req.body;

      if (req.body.location) {
        images = req.body.location;
      }

      const result = await productFacade.addProduct(
        {
          pId,
          name,
          description,
          price,
          images,
          category,
          stock
        }
      );

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

export default productRouter;
