/**
 * This file will have request and response object mappings.
 */
import _ from 'lodash';
import contstants from '../../../constant.js';

const addProduct = (params) => {
  return {
    message: "Product is added sucessfully",
    productDetails: {
      pId: params.pId,
      name: params.name,
      price: params.price,
      images: params.images,
      category: params.category,
      stock: params.stock
    }
  }
}

export default {
  addProduct
};
