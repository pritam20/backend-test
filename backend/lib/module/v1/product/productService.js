//= ========================= Load Modules Start =======================
import appUtils from '../../../appUtils.js';
//= ========================= Load internal modules ====================
import productDao from './productDao.js';

//= ========================= Load Modules End ==============================================

const findProduct = async (params) => {
  return await productDao.findProduct(params)
}

const addProduct = async (params) => {
  return await productDao.addProduct(params)
}

//= ========================= Export Module Start ==============================

export default {
  findProduct,
  addProduct
};

//= ========================= Export Module End ===============================
