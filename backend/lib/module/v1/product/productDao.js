//= ========================= Load Modules Start =======================

//= ========================= Load internal modules ====================
import mongoose from 'mongoose';
import _ from 'lodash';
//= ========================= Load internal modules ====================
import productModel from './productModel.js';

// init user dao
import BaseDao from '../../../dao/baseDao.js';

const productDao = new BaseDao(productModel);
//= ========================= Load Modules End ==============================================

const findProduct = async (pId) => {

  return await productDao.findOne({ pId: pId })
}

const addProduct = async (params) => {

  const query = {
    pId: params.pId ?? null,
    name: params.name ?? null,
    description: params.description ?? null,
    price: params.price ?? null,
    rating: params.rating ?? null,
    images: params.images ?? null,
    category: params.category ?? null,
    stock: params.stock ?? null,
    numOfReview: params.numOfReview ?? null,
    review: params.reviews ?? null
  }

  return productDao.save(query)
}


//= ======================= Export Module Start ==============================

export default {
  findProduct,
  addProduct
};

//= ========================= Export Module End ===============================
