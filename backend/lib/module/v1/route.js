import express from 'express';

import userRoute from './user/userRoute.js';
import adminRoute from './admin/adminRoute.js';
import productRoute from './product/productRoute.js'
import test from './test/testRoute.js'

const router = express.Router();

//= ========================= Export Module Start ==========================

// API version 1
router.use('/user', userRoute);
router.use('/admin', adminRoute);
router.use('/product', productRoute)
router.use('/test', test)

export default router;
//= ========================= Export Module End ============================
