//= ========================= Load Modules Start =======================

//= ========================= Load external modules ====================
// Load user service
import _ from 'lodash';
//= ========================= Load internal modules ====================

import testService from './testService.js';
import testMapper from './testMapper.js';

import appUtils from '../../../appUtils.js';
import redisSession from '../../../redisClient/session.js';
import customException from '../../../customException.js';
import constants from '../../../constant.js';
import emailService from '../../../service/sendgrid_email.js';
import redis from '../../../redisClient/init.js';
import mail from '../../../service/nodemailer_email.js';

//= ========================= Load Modules End ==============================================

const airbnbCount = async (params) => {
  try {

    const count = await testService.airbnbCount()

    return testMapper.airbnbCountMapper()

  }
  catch (err) {
    throw err
  }

}

const topAirbnb = async (params) => {
  try {

    const topAirbnb = await testService.topAirbnb()

    return testMapper.topAirbnbMapper(topAirbnb)

  }
  catch (err) {
    throw err
  }

}

const nearBy = async (params) => {
  try {

    const nearBy = await testService.nearBy(params)

    return testMapper.paramsMapper(nearBy)

  }
  catch (err) {
    throw err
  }
}

//= ========================= Export Module Start ==============================

export default {
  airbnbCount,
  topAirbnb,
  nearBy
};

//= ========================= Export Module End ================================
