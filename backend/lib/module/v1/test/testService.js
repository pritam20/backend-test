//= ========================= Load Modules Start =======================
import appUtils from '../../../appUtils.js';
//= ========================= Load internal modules ====================
import testDao from './testDao.js';
import csv from '../../../service/csv.js'

//= ========================= Load Modules End ==============================================

const airbnbCount = async () => {
  const count = await testDao.airbnbCount()

  return csv.create(count)
}

const topAirbnb = async () => {
  return await testDao.topAirbnb()
}

const nearBy = async (params) => {
  return await testDao.nearBy(params)
}

//= ========================= Export Module Start ==============================

export default {
  airbnbCount,
  topAirbnb,
  nearBy
};

//= ========================= Export Module End ===============================
