import express from 'express';

import resHandler from '../../../responseHandler.js';
import middleware from '../../../middleware/index.js';
import testFacade from './testFacade.js';
import validator from './testValidators.js';

const testRouter = express.Router();

//= =============================================================

testRouter.route('/airbnbCount')
  .get(async (req, res) => {
    try {

      const result = await testFacade.airbnbCount()

      resHandler.sendSuccess(res, result, req);
    } catch (err) {
      resHandler.sendError(res, err, req);
    }
  });

  testRouter.route('/nearBy')
  .get(async (req, res) => {
    try {

      const {lat, long} = req.query

      const result = await testFacade.nearBy({lat, long})

      resHandler.sendSuccess(res, result, req);
    } catch (err) {
      resHandler.sendError(res, err, req);
    }
  });

testRouter.route('/topAirbnb')
  .get(async (req, res) => {
    try {

      const result = await testFacade.topAirbnb()

      resHandler.sendSuccess(res, result, req);
    } catch (err) {
      resHandler.sendError(res, err, req);
    }
  });

export default testRouter;
