/**
 * This file will have request and response object mappings.
 */
import _ from 'lodash';
import contstants from '../../../constant.js';

const airbnbCountMapper = () => {
  return {
    message: "hotels.csv file created successfully. Please check in root folder",
  }
}

const topAirbnbMapper = (params) => {

  return {
    message: "top 5 Airbnb",
    params
  }
}

const paramsMapper = (params) => {
  return params
}

export default {
  airbnbCountMapper,
  topAirbnbMapper,
  paramsMapper
};
