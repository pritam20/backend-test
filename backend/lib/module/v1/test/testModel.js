// Importing mongoose
import mongoose from 'mongoose';
import constants from '../../../constant.js';

const { Schema } = mongoose;
let ListingsAndReviews;

const listingsAndReviewsSchema = new Schema({

},
);

// Export user module
ListingsAndReviews = mongoose.model(constants.DB_MODEL_REF.LISTINGS_AND_REVIEWS, listingsAndReviewsSchema);

export default ListingsAndReviews;
