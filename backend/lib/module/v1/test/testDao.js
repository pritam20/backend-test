//= ========================= Load Modules Start =======================

//= ========================= Load internal modules ====================
import mongoose from 'mongoose';
import _ from 'lodash';
//= ========================= Load internal modules ====================
import testModel from './testModel.js';

// init user dao
import BaseDao from '../../../dao/baseDao.js';

const testDao = new BaseDao(testModel);
//= ========================= Load Modules End ==============================================

const airbnbCount = async (pId) => {

  const data = await testDao.aggregate([
    { $match: { _id: { $ne: '' } } },
    {
      $group: {
        _id: "$address.country",
        count: { $sum: 1 }
      }
    },
    {
      $project: {
        country: "$_id",
        count: 1,
        _id: 0
      }
    }
  ])

  return data
}


const topAirbnb = async () => {

  return await testDao.aggregate([
    { $match: { _id: { $ne: '' } } },
    { $sort: { 'review_scores.review_scores_rating': - 1 } },
    { $skip: 0 },
    { $limit: 5 }
  ])
}

const nearBy = async (params) => {
  return await testDao.find({
    "address.location": {
      $near: {
        $geometry: {
          type: "Point",
          coordinates: [params.long, params.lat]
        },
        $maxDistance: 5000
      }
    }
  }, { name: 1, address: 1, description: 1 });
}

//= ======================= Export Module Start ==============================

export default {
  airbnbCount,
  topAirbnb,
  nearBy
};

//= ========================= Export Module End ===============================
