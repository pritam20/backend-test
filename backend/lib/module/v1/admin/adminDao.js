//= ========================= Load Modules Start =======================

//= ========================= Load internal modules ====================
import mongoose from 'mongoose';
import _ from 'lodash';
//= ========================= Load internal modules ====================
import adminModel from './adminModel.js';

// init admin dao
import BaseDao from '../../../dao/baseDao.js';

const adminDao = new BaseDao(adminModel);

//= ========================= Load Modules End ==============================================

const isEmailExist = async (email) => {
  try {
    return await adminDao.findOne({ email });
  } catch (err) {
    return err;
  }
};

const createAdmin = async (params) => {
  const query = {};

  query.name = params.name,
    query.email = params.email,
    query.password = params.password,
    query.adminname = params.adminname,
    query.adminType = params.adminType,
    query.gender = params.gender,
    query.dob = params.dob,
    query.designation = params.designation,
    query.companyName = params.companyName,
    query.phoneNo = params.phoneNo,
    query.clientIp = params.clientIp;

  return await adminDao.save(query);
};

const deleteadmin = async (email) => await adminDao.deleteOne({ email });

const changePassword = async (params) => await adminDao.findOneAndUpdate({ email: params.email }, { password: params.password });

const getByKey = (query) => adminDao.findOne(query);

//= ========================= Export Module Start ==============================

export default {
  createAdmin,
  changePassword,
  getByKey,
  isEmailExist,
  deleteadmin,
};

//= ========================= Export Module End ===============================
