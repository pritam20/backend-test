//= ========================= Load Modules Start ===========================

//= ========================= Load external Module =========================
import _ from 'lodash';

//= ========================= Load Internal Module =========================
import Joi from 'joi';
import appUtils from '../../../appUtils.js';
import constant from '../../../constant.js';
import exceptions from '../../../customException.js';
//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

const validateCreate = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/).required(),
    adminType: Joi.number().required(),

  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateLogin = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateReset = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
    otp: Joi.string().length(6).pattern(/^\d+$/).required(),
    password: Joi.string().pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$/).required(),
  });

  const { error, value } = schema.validate(req.body);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validateDeleteAccount = (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().required(),
  });

  const { error, value } = schema.validate(req.query);

  if (error && error.details.length > 0) {
    validationError(error.details, next);
  } else {
    next();
  }
};

const validationError = (errors, next) => {
  if (errors && errors.length > 0) {
    return next(exceptions.getCustomErrorException(constant.MESSAGES.VALIDATION_ERROR, errors));
  }
  next();
};

export default {
  validateCreate,
  validateLogin,
  validateReset,
  validateDeleteAccount,
};
//= ========================= Export module end ==================================
