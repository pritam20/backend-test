import express from 'express';
import requestIp from 'request-ip';

import resHndlr from '../../../responseHandler.js';
import middleware from '../../../middleware/index.js';
import adminFacade from './adminFacade.js';
import validators from './adminValidators.js';

const adminRouter = express.Router();

//= =============================================================

adminRouter.route('/create')
  .post(middleware.multer.single('profileImage'), middleware.mediaUpload.uploadSingleMediaToS3('profile'), [validators.validateCreate], async(req, res) => {
    try {
      const {
        email,
        password,
        adminType,
      } = req.body;

      const clientIp = requestIp.getClientIp(req);

      const result = await adminFacade.create({
        email,
        password,
        adminType,
        clientIp,
      });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

adminRouter.route('/login')
  .post([validators.validateLogin], async(req, res) => {
    try {
      const { email, password } = req.body;

      const clientIp = requestIp.getClientIp(req);

      const result = await adminFacade.adminLogin({ email, password, clientIp });

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

adminRouter.route('/logout')
  .post([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], (req, res) => {
    try {
      const result = adminFacade.logout();

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

adminRouter.route('/deleteAccount')
  .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], [validators.validateDeleteAccount], async(req, res) => {
    try {
      const { email } = req.query;

      const result = await adminFacade.deleteAccount(email);

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

// resetPassword
adminRouter.route('/resetPassword')
  .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async(req, res) => {
    try {
      const { email, password } = req.query;

      const result = await adminFacade.resetPassword(email, password);

      resHndlr.sendSuccess(res, result, req);
    } catch (err) {
      resHndlr.sendError(res, err, req);
    }
  });

export default adminRouter;
