//= ========================= Load Modules Start =======================
import appUtils from '../../../appUtils.js';
//= ========================= Load internal modules ====================
import adminDao from './adminDao.js';
import customException from '../../../customException.js';

//= ========================= Load Modules End ==============================================

const isEmailExist = async (email) => {
  return await adminDao.isEmailExist(email)
};

const createAdmin = async (params) => {
  try {
    // checking email is exist or not
    const doesEmailExist = await isEmailExist(params.email);

    // if email is exist and user is verified, user will not able to again signup with the same email
    if (doesEmailExist && doesEmailExist.status == 1) {
      return 0;
    }

    params.password = appUtils.encryptHashPassword(params.password); // change plain password to hash password
    const adminDetails = await adminDao.createAdmin(params);
    adminDetails.otp = appUtils.getRandomOtp(); // add random otp to admindetails
    return adminDetails;
  } catch (err) {
    return err;
  }
};

const changePassword = async (params) => {
  params.password = appUtils.encryptHashPassword(params.password); // change plain password to hash password
  return await adminDao.changePassword(params);
};

const deleteAccount = (email) => adminDao.deleteadmin(email);

const getByKey = (param) => adminDao.getByKey(param);

//= ========================= Export Module Start ==============================

export default {
  createAdmin,
  deleteAccount,
  changePassword,
  isEmailExist,
  getByKey,
};

//= ========================= Export Module End ===============================
