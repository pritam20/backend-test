//= ========================= Load Modules Start ===========================
import { Server } from 'socket.io';
import http from 'http';
import express from 'express';
import { instrument } from '@socket.io/admin-ui';
//= ========================= Load internal Module =========================
import userService from '../module/v1/user/userService.js';
import auth from './socketAuthentication.js';
import config from '../config/index.js';
//= ========================= Load Modules End =============================
const app = express();
const server = http.createServer(app);
server.listen(config.cfg.socket.port);

const io = new Server(server, {
  transports: ['polling', 'websocket'],
  cors: {
    origin: ['http://localhost:4000', config.cfg.webBasePath],
    methods: ['GET', 'POST'],
    credentials: true,
  },
});

instrument(io, {
  auth: {
    type: 'basic',
    username: 'vcards-socket-admin',
    password: '$2b$10$heqvAkYMez.Va6Et2uXInOnkCT6/uQj1brkrbyG3LpopDklcq7ZOS', // "changeit" encrypted with bcrypt
  },
});

// socket access token authenticate
io.use(auth.authSocketTkn);

io.sockets.on('connection', (socket) => {
  console.log('*** user ***');
  console.log('socket.id', socket.id);

  if (socket.user) {
    addSocketID(socket);
    socketDisconnections(socket);
    // groupAdd(socket);
    // socketDisconnectionsGroup(socket);
    // groupMessaging(socket);
    // messaging(socket);
  }
});

const addSocketID = async(socket) => {
  try {
    const userId = socket.user._id;

    if (userId && socket.payload.isAdmin == 0) {
      const updateUser = await userService.updateUser({ _id: userId }, { $addToSet: { socketId: socket.id } });
      if (updateUser) {
        console.log('socket Id add');
        // console.log(user);
        socket.broadcast.emit('online', { user });
      } else {
        console.log('invalid user id');
        console.log(userId);
      }
    } else {
      const userDetail = adminService.update({ _id: userId }, { $addToSet: { socketId: socket.id } });
      if (userDetail) {
        console.log('socket Id add');
        // console.log(user);
        socket.broadcast.emit('online', { user });
      } else {
        console.log('invalid user id');
        console.log(userId);
      }
    }
  } catch (err) {
    throw err;
  }
};

const groupAdd = (socket) => {
  try {
    socket.on('groupAdd', async(matchId) => {
      const group = await groupService.update({ matchId }, { $addToSet: { socketId: socket.id, userId: socket.user._id } });
      console.log('group', group);
      if (group) {
        console.log('socket Id add in group');
        await socket.join(group._id, () => {
          const rooms = Object.keys(socket.rooms);
          console.log('rooms', rooms); // [ <socket.id>, 'room 237' ]
        });
      }
    });
  } catch (err) {
    throw err;
  }
};

const socketDisconnections = (socket) => {
  try {
    socket.on('disconnect', async() => {
      const user = await userService.updateUser({ socketId: socket.id }, { $pull: { socketId: socket.id } });
      console.log('user disconnected');
      socket.broadcast.emit('offline', { user });
    });
  } catch (err) {
    throw err;
  }
};

const socketDisconnectionsGroup = (socket) => {
  try {
    socket.on('disconnect', async() => {
      const user = await groupService.update({ socketId: socket.id }, { $pull: { socketId: socket.id } });
      console.log('user disconnected from group');
    });
  } catch (err) {
    throw err;
  }
};

const groupMessaging = (socket) => {
  try {
    socket.on('groupSend', async(msg, groupId, matchId) => {
      const group = await groupService.getByKey({ _id: groupId, matchId });
      const socketArr = group.socketId;
      // groupId => chat room id
      await socket.broadcast.to(groupId).emit('groupMsg', { msg, group: groupId, user: socket.user });

      const chatMsg = {
        userId: socket.user._id,
        matchId,
        groupId,
        message: msg,
        created: new Date(),
        isRemove: 0,
        status: 1,
      };
      return messageService.create(chatMsg);
    });
  } catch (err) {
    throw err;
  }
};

const messaging = (socket) => {
  try {
    socket.on('messageSend', async(msg, userId) => {
      const toUser = await userService.getByKey({ _id: userId });

      const socketArr = toUser.socketId;

      for (let i = 0; i < socketArr.length; i++) {
        socket.broadcast.to(socketArr[i]).emit('messageRecive', { msg, name: toUser.name });
      }

      const chatMsg = {
        userId: socket.user._id,
        toUserId: userId,
        message: msg,
        created: new Date(),
        isRemove: 0,
        status: 1,
      };
      return messageService.create(chatMsg);
    });
  } catch (err) {
    throw err;
  }
};

export default {

};
